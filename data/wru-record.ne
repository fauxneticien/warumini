@{%

    const token_matcher = function(key_name) {
        // Nearley calls the function assigned to the "test" attribute
        // of an object, i.e. { test: function({}) => true/false }
        // to parse a stream of objects
        // https://nearley.js.org/docs/tokenizers#custom-token-matchers
        return {
            test: function(token) { return token.key == key_name }
        }
    }

    const cf = token_matcher("cf")
    const ocf = token_matcher("ocf")
    const ucf = token_matcher("ucf")
    const uocf = token_matcher("uocf")
    const w = token_matcher("w")
    const ow = token_matcher("ow")
    const sw = token_matcher("sw")
    const sow = token_matcher("sow")
    const v = token_matcher("v")
    const ov = token_matcher("ov")
    const sv = token_matcher("sv")
    const sov = token_matcher("sov")
    const uv = token_matcher("uv")
    const uov = token_matcher("uov")
    const g = token_matcher("g")
    const og = token_matcher("og")
    const sg = token_matcher("sg")
    const sog = token_matcher("sog")
    const img = token_matcher("img")
    const oimg = token_matcher("oimg")
    const simg = token_matcher("simg")
    const soimg = token_matcher("soimg")
    const audio = token_matcher("audio")
    const oaudio = token_matcher("oaudio")
    const saudio = token_matcher("saudio")
    const soaudio = token_matcher("soaudio")
    const sn = token_matcher("sn")
    const ssn = token_matcher("ssn")
    const p = token_matcher("p")
    const sp = token_matcher("sp")
    const c = token_matcher("c")
    const sc = token_matcher("sc")
    const r = token_matcher("r")
    const or = token_matcher("or")
    const sr = token_matcher("sr")
    const sor = token_matcher("sor")
    const cw = token_matcher("cw")
    const ocw = token_matcher("ocw")
    const d = token_matcher("d")
    const od = token_matcher("od")
    const sd = token_matcher("sd")
    const sod = token_matcher("sod")
    const gl = token_matcher("gl")
    const ogl = token_matcher("ogl")
    const sgl = token_matcher("sgl")
    const sogl = token_matcher("sogl")
    const l = token_matcher("l")
    const ol = token_matcher("ol")
    const ul = token_matcher("ul")
    const uol = token_matcher("uol")
    const sl = token_matcher("sl")
    const sol = token_matcher("sol")
    const f = token_matcher("f")
    const of = token_matcher("of")
    const sf = token_matcher("sf")
    const sof = token_matcher("sof")
    const ae = token_matcher("ae")
    const oae = token_matcher("oae")
    const sae = token_matcher("sae")
    const soae = token_matcher("soae")
    const y = token_matcher("y")
    const oy = token_matcher("oy")
    const uk = token_matcher("uk")
    const uok = token_matcher("uok")
    const i = token_matcher("i")
    const oi = token_matcher("oi")
    const t = token_matcher("t")
    const ot = token_matcher("ot")
    const si = token_matcher("si")
    const soi = token_matcher("soi")
    const st = token_matcher("st")
    const sot = token_matcher("sot")
    const ui = token_matcher("ui")
    const uoi = token_matcher("uoi")
    const ut = token_matcher("ut")
    const uot = token_matcher("uot")
    const usi = token_matcher("usi")
    const usoi = token_matcher("usoi")
    const ust = token_matcher("ust")
    const usot = token_matcher("usot")
    const b = token_matcher("b")
    const ob = token_matcher("ob")
    const bt = token_matcher("bt")
    const ub = token_matcher("ub")
    const uob = token_matcher("uob")
    const ubt = token_matcher("ubt")
    const usb = token_matcher("usb")
    const usob = token_matcher("usob")
    const usbt = token_matcher("usbt")
    const q = token_matcher("q")
    const oq = token_matcher("oq")
    const qa = token_matcher("qa")
    const oqa = token_matcher("oqa")
    const qj = token_matcher("qj")
    const oqj = token_matcher("oqj")
    const sq = token_matcher("sq")
    const soq = token_matcher("soq")
    const sqa = token_matcher("sqa")
    const soqa = token_matcher("soqa")
    const sqj = token_matcher("sqj")
    const soqj = token_matcher("soqj")
    const uq = token_matcher("uq")
    const uoq = token_matcher("uoq")
    const a = token_matcher("a")
    const cr = token_matcher("cr")
    const e = token_matcher("e")
    const sy = token_matcher("sy")
    const sa = token_matcher("sa")
    const scr = token_matcher("scr")
    const scf = token_matcher("scf")
    const se = token_matcher("se")
    const uy = token_matcher("uy")
    const ua = token_matcher("ua")
    const ucr = token_matcher("ucr")
    const ue = token_matcher("ue")
    const usq = token_matcher("usq")
    const usw = token_matcher("usw")
    const usow = token_matcher("usow")

%}

record ->
      headword
    | subEntry
    | unverifiedSubEntry

headword ->
    lexeme
    variant:*
    grammaticalNote:*
    image:*
    audio:*
    comments:?
    unverifiedEntryData:?
    senseItem:+

senseItem ->
    senseLabel
    image:*
    audio:*
    partOfSpeech
    grammaticalNote:*
    semanticDomain
    register:?
    warumunguClassifications:*
    definition
    reversalGloss:*
    aboriginalEnglish:*
    latinGloss:*
    literalGloss:?
    examples:?
    oldSourceGroup:*
    senseRelations:?
    comments:?
    unverifiedData:?

subEntry ->
    sublexeme
    subVariant:*
    subGrammaticalNote:*
    subImage:*
    subAudio:*
    subSenseItem:+

subSenseItem ->
    subSenseLabel
    subImage:*
    subAudio:*
    subPartOfSpeech:+
    subGrammaticalNote:*
    subSemanticDomain
    subRegister:?
    subDefinition
    subReversalGloss:*
    subAboriginalEnglish:*
    subLatinGloss:?
    subLiteralGloss:?
    subExamples:?
    subSenseRelations:?
    subComments:?
    subUnverifiedData:?
    
unverifiedSubEntry ->
    unverifiedSubLexeme

compare ->
    %cf   %ocf:?
    
unverifiedCompare ->
    %ucf   %uocf:?

lexeme ->
    %w     %ow:*

sublexeme ->
    %sw    %sow:*

variant ->
    %v     %ov:*

subVariant ->
    %sv    %sov:*
    
unverifiedVariant ->
    %uv    %uov:?

grammaticalNote ->
    %g     %og:*
    
subGrammaticalNote ->
    %sg    %sog:*

image ->
    %img   %oimg:*

subImage ->
    %simg  %soimg:*

audio ->
    %audio %oaudio

subAudio ->
    %saudio %soaudio

senseLabel ->
    %sn

subSenseLabel ->
    %ssn    

partOfSpeech ->
    %p

subPartOfSpeech ->
    %sp

semanticDomain ->
    %c

subSemanticDomain ->
    %sc

register ->
    %r     %or:*

subRegister ->
    %sr    %sor:*

warumunguClassifications ->
    %cw    %ocw

definition ->
    %d     %od:*

subDefinition ->
    %sd    %sod:*

reversalGloss ->
    %gl    %ogl:?

subReversalGloss ->
    %sgl    %sogl:?
    
latinGloss ->
    %l     %ol:*
    
unverifiedLatinGloss ->
    %ul     %uol:?

subLatinGloss ->
    %sl    %sol:*

literalGloss ->
    %f     %of:*

subLiteralGloss ->
    %sf    %sof:*   

aboriginalEnglish ->
    %ae    %oae:*
    
subAboriginalEnglish ->
    %sae    %soae:*

synonym ->
    %y     %oy:?

wirnkarraInformation ->
    %uk     %uok:?

examples ->
    exampleGroup:+

subExamples ->
    subExampleGroup:+

exampleGroup ->
    %i
    %oi:+
    audio:?
    %t
    %ot:*

subExampleGroup ->
    %si
    %soi:+
    subAudio:?
    %st
    %sot:*

unverifiedExampleGroup ->
    %ui
    %uoi:+
    %ut:?
    %uot:*

subUnverifiedExampleGroup ->
    %usi
    %usoi:+
    %ust:?
    %usot:*

oldSourceGroup ->
    %b
    %ob
    %bt

unverifiedOldSourceGroup ->
    %ub
    %uob
    %ubt:?

subUnverifiedOldSourceGroup ->
    %usb
    %usob
    %usbt:?

comments ->
    commentGroup:+

commentGroup ->
    # Comment + optional comment source: \X + 0 or more \oX
      (%q    %oq:*)
    | (%qa   %oqa:*)
    | (%qj   %oqj:*)

subComments ->
    subCommentGroup:+

subCommentGroup ->
    # Comment + optional comment source: \X + 0 or more \oX
      (%sq    %soq:*)
    | (%sqa   %soqa:*)
    | (%sqj   %soqj:*)

unverifiedComment ->
    %uq      %uoq:?    

senseRelations  ->
    synonym:*
    %a:*
    %cr:*
    compare:*
    %e:*

subSenseRelations  ->
    %sy:*
    %sa:*
    %scr:*
    %scf:*
    %se:*

unverifiedSenseRelations  ->
    %uy:*
    %ua:*
    %ucr:*
    unverifiedCompare:*
    %ue:*

unverifiedEntryData ->
    unverifiedVariant:*
    %uq:*

unverifiedData ->
    # All backslash codes starting with 'u'. Place below in alphabetical order.
    unverifiedComment:*
    unverifiedOldSourceGroup:*
    unverifiedExampleGroup:*
    unverifiedLatinGloss:*
    unverifiedSenseRelations:?
    wirnkarraInformation:*

subUnverifiedData ->
    %usq:*
    subUnverifiedOldSourceGroup:*
    subUnverifiedExampleGroup:*
    
unverifiedSubLexeme ->
    %usw   %usow:?
