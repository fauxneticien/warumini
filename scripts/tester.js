const fs       = require('fs')

const _        = require('lodash')
const getStdin = require('get-stdin')
const nearley  = require('nearley')

const tests    = [
	{
		"level": 0,
		"test" : function(key_array) {
			return(key_array.length)
		}
	}
]

var stdin_str      = fs.readFileSync('../_artifacts/wru-vocab-tokenized.json').toString()

var lexicon_nested = JSON.parse(stdin_str)

var headwords_list = _(lexicon_nested).flattenDeep().filter(o => o.key == "w").map(o => o.value).value()

// const grammar  = require('../_artifacts/wru-record.js')

// var parser = new nearley.Parser(nearley.Grammar.fromCompiled(grammar))

// parser.feed(records[0])

// console.log(parser.results[0])

// getStdin().then(
// 	stdin_str => (
// 		console.log(JSON.parse(stdin_str).length)
// 	)
// )

// [
// 	[],
// 	[]
// ]