const lexdev      = require('./lexdev.js')
const getStdin    = require('get-stdin')

const split_funcs = [
	// split whole lexicon into records "\\lx ... \\lx" => ["\\lx ", ..., "\\lx"]
	lexicon_str   => lexicon_str.split(/\n*[\s\t]*(?=\\u?s?w\s)/),

	// splits a string "\\lx bla ... \ps bla" => ["\\lx bla", "\\ps bla"]
	record_str    => record_str.split(/\n?[\s\t]*(?=\\[a-z|_]+)/),

	// convert token strings, i.e. ["\ps Noun", ...] =>
	// JSON objects [{ "type" : "ps", "value" : "Noun" }, ... ]
	kv_str        => ({   "key" : kv_str.match(/\\([a-z|_]+)\s([\s\S]*)/)[1],
		                "value" : kv_str.match(/\\([a-z|_]+)\s([\s\S]*)/)[2] })
]

getStdin().then(
	stdin_str => (
		process.stdout.write(
			JSON.stringify(lexdev.ecuder(split_funcs)(stdin_str))
		)
	)
)
